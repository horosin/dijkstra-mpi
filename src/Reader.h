#pragma once
#include <iostream>
#include "slistEl.h"

using namespace std;

class Reader {

    int n;
    int m;

    int *edges;

public:

    Reader() {
        
        edges = new int[m*3];
    }

    void read() {
        cin >> n >> m;

        readInput();
    }

    void readInput() {

        int w, x, y;

        for(int i = 0; i < m; i++) {

            cin >> x >> y >> w;

            edges[i*3 + 0] = x;
            edges[i*3 + 1] = y;
            edges[i*3 + 2] = w;

        }

    }


    int getN() { return n; }
    int getM() { return m; }

    int* getEdges() { return edges; }
    

};