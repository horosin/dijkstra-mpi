#include <iostream>
#include "mpi.h"

#include "Reader.h"
#include "Dijkstra.h"

using namespace std;

int main(int argc, char* argv[])
{

    // init mpi
    int myid, numprocs;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);


    cout << "Process: " << myid << ": Im alive" << endl;

    int n, m;
    int* edges;

    if (myid == 0) {
        // master node actions

        cout << "Number of available processes: " << numprocs << endl << endl;

        Reader reader;
        reader.read();

        n = reader.getN();
        m = reader.getM();
        edges = reader.getEdges();
    }

    cout << "Process: " << myid << ": before bcast" << endl;

    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD );
    MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD );
    MPI_Bcast(edges, m*3, MPI_INT, 0, MPI_COMM_WORLD );


    cout << "Process: " << myid << ": after bcast" << endl;

    Dijkstra runner(
        n,
        m,
        edges
    );
    runner.run(0);
  
    
    cout << "Process: " << myid << ": after dijkstra" << endl;

    MPI_Finalize();
    return 0;
} 