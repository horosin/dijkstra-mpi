#pragma once
#include <iostream>
#include "slistEl.h"

using namespace std;

const int MAXINT = 2147483647;


class Dijkstra {

    int m, n;    // edges, vertices
    int v;      // source
    int *d, *p, *S;
    bool *QS;                                        // Zbiory Q i S
    slistEl **graf;                                 // Tablica list sąsiedztwa

public:

    Dijkstra(int n, int m, int* edge_buffer)
        : n(n)
        , m(m)
    {
    
        initStructures(n);

        edgesToGraph(edge_buffer);

    }

    void edgesToGraph(int* edge_buffer) {

        // convert edge buffer to array just for convienience
        int **edges = new int* [m];
        for(int i = 0; i < m; ++i)
            edges[i] = edge_buffer + i * 3;

        slistEl *pw;

        for(int i = 0; i < m; i++) {

            pw = new slistEl;             // Tworzymy element listy sąsiedztwa
            pw->v = edges[i][1];                    // Wierzchołek docelowy krawędzi
            pw->w = edges[i][2];                    // Waga krawędzi
            pw->next = graf[edges[i][0]];
            graf[edges[i][0]] = pw;                 // Element dołączamy do listy
        }

        cout << endl;

    }

    void initStructures(int n) {
        // Tworzymy tablice dynamiczne

        d = new int [n];                                // Tablica kosztów dojścia
        p = new int [n];                                // Tablica poprzedników
        QS = new bool [n];                              // Zbiory Q i S
        S = new int [n];                                // Stos
        graf = new slistEl * [n];       // Tablica list sąsiedztwa


        // Inicjujemy tablice dynamiczne

        for(int i = 0; i < n; i++) {
            d[i] = MAXINT;
            p[i] = -1;
            QS[i] = false;
            graf[i] = NULL;

        }
    }

    void findPaths() {

        slistEl *pw;
        int u;

        for(int i = 0; i < n; i++) {
            // Szukamy wierzchołka w Q o najmniejszym koszcie d

            int j;
            for(j= 0; QS[j]; j++);
            for(u = j++; j < n; j++)
                if(!QS[j] && (d[j] < d[u])) u = j;

            // Znaleziony wierzchołek przenosimy do S

            QS[u] = true;

            // Modyfikujemy odpowiednio wszystkich sąsiadów u, którzy są w Q

            for(pw = graf[u]; pw; pw = pw->next)
                if(!QS[pw->v] && (d[pw->v] > d[u] + pw->w))
                {
                    d[pw->v] = d[u] + pw->w;
                    p[pw->v] = u;
                }
        }

    }

    void printResult() {

        int sptr = 0;

        for(int i = 0; i < n; i++)
        {
            cout << i << ": ";

            // Ścieżkę przechodzimy od końca ku początkowi,
            // Zapisując na stosie kolejne wierzchołki

            for(int j = i; j > -1; j = p[j]) S[sptr++] = j;

            // Wyświetlamy ścieżkę, pobierając wierzchołki ze stosu

            while(sptr) cout << S[--sptr] << " ";

            // Na końcu ścieżki wypisujemy jej koszt

            cout << "$" << d[i] << endl;
        }

    }

    void run(int v) {

        d[v] = 0;                                             // Koszt dojścia v jest zerowy

        findPaths();
        printResult();

    }

    ~Dijkstra() {

        // Usuwamy tablice dynamiczne
        slistEl *pw,*rw;

        delete [] d;
        delete [] p;
        delete [] QS;
        delete [] S;

        for(int i = 0; i < n; i++)
        {
            pw = graf[i];
            while(pw)
            {
                rw = pw;
                pw = pw->next;
                delete rw;
            }
        }

        delete [] graf;

    }
};