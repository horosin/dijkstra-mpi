# dijkstra-mpi

MPI distributed implementation of well-known algorithm.

## Prerequisites

1. gcc compiler
2. mpich https://www.mpich.org/downloads/

To install mpich, just
``` sudo apt-get update && apt-get install mpich ```
or
``` brew install mpich ```


## Usage

To compile and test:
```
make all
```